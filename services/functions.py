from database.komercio import lista_de_produtos
from flask import jsonify

def list_products(page, per_page):
    ultimo_item = per_page * page
    primeiro_item = ultimo_item - per_page
    if(ultimo_item <= len(lista_de_produtos)):
        new_list = [item for item in lista_de_produtos if item['id'] -1 >= primeiro_item and item['id'] - 1 <= primeiro_item + per_page - 1  ]
        return jsonify(new_list)
    return ('', 404)

def get_product(product_id):
    for product in lista_de_produtos:
        if product['id'] == int(product_id):
            return product

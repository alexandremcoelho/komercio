from flask import Flask, jsonify, request
from database.komercio import lista_de_produtos
from flask_cors import CORS
from services.functions import list_products, get_product

app = Flask(__name__)
CORS(app)

@app.route('/products')
def get_per_page():
    page = request.args.get('page')
    per_page = request.args.get('per_page')
    if(page and per_page):
        filtered_by_name = list_products(int(page), int(per_page))
        return filtered_by_name
    return jsonify(lista_de_produtos)

@app.route('/products/<product_id>')
def get_one_product(product_id):
        product = get_product(product_id)
        if product:
            return product
        return ('', 404)

